"""Runs before cookiecutter project is generated.

This script validates some of the input cookiecutter variables defined in
``cookiecutter.json``.
"""

import re
import sys


def check_package_name():
    """Checks cookiecutter package name is PEP8 compliant.

    Raises
    ------
    SystemExit
        if the package name is invalid.

    References
    ----------
    .. [1] https://cookiecutter.readthedocs.io/en/1.7.3/advanced/hooks.html#example-validating-template-variables
    """

    _PACKAGE_REGEX = r"^[_a-zA-Z][_a-zA-Z0-9]+$"
    
    package_name = "{{ cookiecutter.package_name }}"

    if not re.match(_PACKAGE_REGEX, package_name):
        print(f"ERROR: {package_name} is not a valid Python package name!")

        # exits with status 1 to indicate failure
        sys.exit(1)


def check_gitlab_subpath():
    """Checks cookiecutter gitlab subpath is valid.

    Raises
    ------
    SystemExit
        if the gitlab subpath is invalid.

    """
    
    path = "{{ cookiecutter.gitlab_subpath }}"

    if path != "" and not path.endswith('/'):
        print(f"ERROR: {path} is not a valid subpath to a gitlab group. Make sure that the path ends with `/`.")

        # exits with status 1 to indicate failure
        sys.exit(1)

def check_semantic_versioning():
    """Checks cookiecutter version number follows semantic versioning.

    Raises
    ------
    SystemExit
        if the version number is invalid.

    References
    ----------
    .. [1] https://semver.org/
    """

    _SEMVER_REGEX = r"^(?P<major>0|[1-9]\d*)\.(?P<minor>0|[1-9]\d*)\.(?P<patch>0|[1-9]\d*)(?:-(?P<prerelease>(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+(?P<buildmetadata>[0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$"

    version = "{{ cookiecutter.version }}"

    if not re.match(_SEMVER_REGEX, version):
        print(f"ERROR: {version} is not a valid semantic versioning version name!")

        # exits with status 1 to indicate failure
        sys.exit(1)


def check_package_registry_id():
    """Checks cookiecutter shared package registry id is valid.

    Raises
    ------
    SystemExit
        if the package_registry id is invalid.
    """

    pr_id = "{{ cookiecutter.shared_package_registry_id }}"

    if pr_id != "":
        
        try:
            int(pr_id)
        
        except ValueError:
            print(f"ERROR: {pr_id} is not a valid shared package registry id!")

            # exits with status 1 to indicate failure
            sys.exit(1)


if __name__ == "__main__":

    print("\n✨COOKIECUTTER PRE-GEN HOOKS✨")

    check_package_name()

    check_gitlab_subpath()

    check_semantic_versioning()

    check_package_registry_id()

    print("\n")