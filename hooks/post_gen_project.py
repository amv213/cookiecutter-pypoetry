import subprocess
import sys
import textwrap

try:
    import pre_commit
except ImportError:
    message = """\
    \n❌ Failed to import the 'pre-commit' package.\n
    Please install it using the following command:\n
    \t$ python -m pip install pre_commit\n"""
    raise SystemExit(textwrap.dedent(message))

_SSH = "{{ cookiecutter.gitlab_ssh_enabled }}"
_GIT_REPO = "{{ cookiecutter.repository }}"
if _SSH == "Y":
    _GIT_REPO = _GIT_REPO.replace("https://gitlab.com/", "git@gitlab.com:")


def git_init():
    """Initialise gitlab version control of the project."""

    try:

        print("\n🌈 initialising git repository:\n")

        subprocess.call(['git', 'init'])
        subprocess.call(['git', 'checkout', '-b', 'main'])
        
        subprocess.call(['git', 'remote', 'add', 'origin', _GIT_REPO])

        subprocess.call(['git', 'add', '.'])
        subprocess.call(['git', 'commit', '-m', 'cookiecutter initial commit'])
    
    except Exception:
        print(f"ERROR: something went wrong when initialising the git repository!")

        # exits with status 1 to indicate failure
        sys.exit(1)


def poetry_init():
    """Install cookiecutter project and dependencies on creation.

    Pre-commit hooks for this project rely on development dependencies
    being available in the local poetry environment.
    """

    try:

        print("\n🚀 initialising poetry environment:\n")

        subprocess.call(['poetry', 'install'])

    except Exception:
        print(f"ERROR: something went wrong when installing poetry project dependencies!")

        # exits with status 1 to indicate failure
        sys.exit(1)


def pre_commit_init():
    """Automatically install and initialise pre-commit hooks in project repository.

    Any newly cloned repository will automatically have the hooks set up
    without the need to run ``pre-commit install``.

    """

    try:

        print("\n🌟 initialising pre-commit hooks:\n")

        subprocess.call(['python', '-m', 'pre_commit', 'install'])
        subprocess.call(['python', '-m', 'pre_commit', 'run', '--all-files'])

    except Exception:
        print(f"ERROR: something went wrong when installing pre-commit hooks!")

        # exits with status 1 to indicate failure
        sys.exit(1)


if __name__ == "__main__":

    print("\n✨COOKIECUTTER POST-GEN HOOKS✨")
    
    git_init()

    poetry_init()

    pre_commit_init()

    print("\n")
