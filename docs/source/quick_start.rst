Quick Start
===========

Make sure you have installed  ``cookiecutter`` on your system from the Python Package Index:

.. code-block:: bash

    $ pip install cookiecutter

Then start your new project based on this ``cookiecutter`` template:

.. code-block:: bash

    $ python -m cookiecutter https://gitlab.com/amv213/cookiecutter-pypoetry.git

    # ... fill out interactive prompt ...

You can now get to work!

.. hint::

    Look at the ``Development`` section in the template documantation of your newly installed package for more help on how to start developing your codebase.