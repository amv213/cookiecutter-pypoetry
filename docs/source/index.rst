.. this file should at least contain the root `toctree` directive.

Welcome to the Docs!
====================

Cookiecutter template for a Python poetry package.

**Features:**

- Modern development ecosystem with ``poetry``
- ``PEP 508`` packaging and dependency management with ``pyproject.toml``
- Continuous integration and development with gitlab pipelines
- Automated packaging to private gitlab PyPI package registry
- Bootstrapped documentation with ``sphinx`` and ``sphinx-book-theme``
- Automatic API documentation generation with ``autodoc`` and ``napoleon``
- Testing with ``pytest``
- Test automation with ``nox``
- Code coverage with ``coverage``
- Linting with ``pre-commit`` and ``flake8``
- Code formatting with ``black``
- Command-line interface with ``click``
- Static type-checking with ``mypy``
- Security audit with ``bandit`` and ``safety``
- Check documentation examples with ``xdoctest``
- Automatic documentation hosting with gitlab pages

This template supports Python ``^3.8``

Table of Contents
--------------------

.. toctree::
   :caption: MAIN DOCS
   :maxdepth: 2

   Quick Start <quick_start.rst>
   Cookiecutter Prompts <prompts.rst>

.. toctree::
   :caption: TUTORIALS
   :maxdepth: 2

   Private Package Repository <pypi.rst>
   GitLab Pipeline <pipeline.rst> 
