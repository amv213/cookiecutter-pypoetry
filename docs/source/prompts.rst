Cookiecutter Prompts
====================

The ``cookiecutter.json`` file defines a number of customisable parameters prompted when initialising a new project with a ``cookiecutter`` template:

.. code-block:: bash

    $ python -m cookiecutter https://gitlab.com/amv213/cookiecutter-pypoetry.git

    # ... start of interactive prompt ...

A short description of each parameter is provided here.

author
------
The author of the package.

email
------
The email of the author of the package.

gitlab_ssh_enabled
------------------
wether you have configured ssh on gitlab.

gitlab_namespace
----------------
name of the gitlab group this project will be under, if any. Else, the gitlab username of the author of the package.

gitlab_subpath
----------------
gitlab slug of the gitlab subgroups this project will be under, if any.

For example, ``my/subgroup/``.

project_name
------------
The name of the project.

project_slug
------------
The name of the root project directory.

package_name
------------
The name of the package.

description
------------
A short description of the package.

version
------------
The version of the package.

This must follow `semantic versioning <https://semver.org/>`_.

license
------------
The license of the package.

The recommended notation for the most common licenses is (alphabetical):

- Apache-2.0
- BSD-2-Clause
- BSD-3-Clause
- BSD-4-Clause
- GPL-2.0-only
- GPL-2.0-or-later
- GPL-3.0-only
- GPL-3.0-or-later
- LGPL-2.1-only
- LGPL-2.1-or-later
- LGPL-3.0-only
- LGPL-3.0-or-later
- MIT

If your project is proprietary and does not use a specific licence, you can set this value as ``Proprietary``.

homepage
------------
An URL to the website of the project. 

repository
------------
An URL to the repository of the project.

documentation
-------------
An URL to the documentation of the project. 

shared_package_registry_id
--------------------------
ID of the gitlab project used as a shared private gitlab package registry, if any.