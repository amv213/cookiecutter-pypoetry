cookiecutter-pypoetry
=======================

Cookiecutter template for Python poetry packages.

Quick Start
-----------

Make sure you have installed  ``cookiecutter`` on your system from the Python Package Index:

.. code-block:: bash

    $ pip install cookiecutter

Then start your new project based on this ``cookiecutter`` template:

.. code-block:: bash

    $ python -m cookiecutter https://gitlab.com/amv213/cookiecutter-pypoetry.git

    # ... fill out interactive prompt ...

Documentation
-------------
 
`Read the docs <https://amv213.gitlab.io/cookiecutter-pypoetry/>`_!