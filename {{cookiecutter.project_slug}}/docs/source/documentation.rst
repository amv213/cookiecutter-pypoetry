Documentation
================

.. toctree::
   :maxdepth: 4

   Modules <modules.rst>

.. note::

    This library implements logging for many of its functions via the Python standard library ``logging`` package. If your using application does not use logging, then only events of severity ``WARNING`` and greater will be printed to ``sys.stderr``.
