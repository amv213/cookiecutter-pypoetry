"""Tests components of the :mod:{{ cookiecutter.package_name }}.utils module."""

import logging

import pytest

from {{ cookiecutter.package_name }} import utils


def test_getBasicLogger_return_type() -> None:
    """It returns a logger."""

    logger = utils.getBasicLogger()

    assert type(logger) == logging.Logger


def test_getBasicLogger_lowercase_level() -> None:
    """It accepts a lowercase level parameter."""

    logger = utils.getBasicLogger(level="warning")

    level_numeric_value = logger.getEffectiveLevel()
    level_as_string = logging.getLevelName(level_numeric_value)

    assert level_as_string == "WARNING"


def test_getBasicLogger_unknown_level() -> None:
    """It returns an error when trying to set unknown level."""

    with pytest.raises(ValueError):
        utils.getBasicLogger(level="MEDIUM")
