"""Tests components of the :mod:{{ cookiecutter.package_name }}.__init__ module."""

from {{ cookiecutter.package_name }} import __version__, package_version


def test_version() -> None:
    """Package has expected version."""

    assert __version__ == package_version()
