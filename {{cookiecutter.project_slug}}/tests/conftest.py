"""Configures global ``pytest`` settings and fixtures."""

import click.testing
import pytest


@pytest.fixture  # type: ignore
def runner() -> click.testing.CliRunner:
    """Spawns a CLI runner.

    The CLI runner provides functionality to invoke a Click command
    line script for unittesting purposes in an isolated environment.

    Returns
    -------
    click.testing.CliRunner
        CLI runner.
    """

    return click.testing.CliRunner()
