"""Tests components of the :mod:{{ cookiecutter.package_name }}.cli module."""

import click.testing

from {{ cookiecutter.package_name }} import cli


def test_main_succeeds(runner: click.testing.CliRunner) -> None:
    """Tests whether the program exits with a status code of zero.

    Parameters
    ----------
    runner : click.testing.CliRunner
        CLI runner
    """

    result = runner.invoke(cli.main)

    assert result.exit_code == 0
