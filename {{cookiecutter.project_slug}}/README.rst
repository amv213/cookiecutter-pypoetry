{{ cookiecutter.project_name }}
===============================
.. image:: https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white
   :target: https://github.com/pre-commit/pre-commit
   :alt: pre-commit

.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
   :target: https://github.com/psf/black

{{ cookiecutter.description }}

Quick Start
-----------

With ``poetry`` installed, you can run the code in this project as follows:

.. code-block:: bash

    # go to your root project directory
    $ cd {{ cookiecutter.project_slug }}

    # install the package and its core dependencies in a virtual environment
    $ poetry install --no-dev

    # test your code in the REPL
    $ poetry run python
    >>> import {{ cookiecutter.package_name }}

Documentation
-------------

`Read the docs <{{ cookiecutter.documentation }}>`_!
