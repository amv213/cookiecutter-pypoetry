"""Initialises the package."""

import logging


def package_version() -> str:
    """Source the package version from the ``pyproject.toml`` file.

    Returns
    -------
    str
        package version.
    """

    from importlib.metadata import version

    return version(__name__)


__version__ = package_version()

# setup package-level logger
logger = logging.getLogger(__name__)
logger.setLevel("WARNING")

# Uncomment if want to prevent the library’s logged events being output to
# sys.stderr in the absence of user-side logging configuration
# logger.addHandler(logging.NullHandler())
