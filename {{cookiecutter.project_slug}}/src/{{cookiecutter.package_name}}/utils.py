"""Contains general-purpose utility functions."""

import logging
from typing import Optional

# Spawn module-level logger
logger = logging.getLogger(__name__)


def getBasicLogger(
    name: Optional[str] = None, level: str = "WARNING"
) -> logging.Logger:
    """Instantiate a basic root logger ready for use by the client application.

    Parameters
    ----------
    name : str, optional
        name of the logger, by default ``__name__`` - the root logger of the
        hierarchy
    level : str, optional
        logging level, by default "WARNING"

    Returns
    -------
    logging.Logger
        logger.

    Examples
    --------
    >>> from {{ cookiecutter.package_name }} import utils
    >>> logger = utils.getBasicLogger(level="INFO")
    >>> logger.info("Hello, world!")
    """

    # Setup basic formatting for log records
    logging.basicConfig(
        format="[%(asctime)s] %(levelname)-8s | %(message)s",
        datefmt="%D %H:%M:%S",
    )

    # Instantiate logger
    logger = logging.getLogger(name or __name__)
    logger.setLevel(level.upper())

    return logger
