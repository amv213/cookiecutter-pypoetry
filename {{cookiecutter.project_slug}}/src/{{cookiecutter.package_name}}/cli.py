"""Contains a demo template for a Command Line Interface (CLI) script.

Examples
--------
Register the ``main()`` function in your ``pyproject.toml`` file:

.. code-block::

    [tool.poetry.scripts]
    <script_name> = '{{ cookiecutter.package_name }}.cli:main'

and run it with:

.. code-block:: bash

    poetry run <script_name> --help
"""

import click


@click.command()
def main() -> None:
    """Run a sample Command Line Interface (CLI) script."""

    click.echo("✨ Welcome to the sample CLI utility! ✨")
